package VueControleur;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;

import modele.Meteo.Ciel;
import modele.Ordonnanceur;
import modele.SimulateurPotager;
import modele.environnement.*;
import modele.environnement.varietes.Legume;
import modele.environnement.varietes.Varietes;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle
 *
 */
public class VueControleurPotager extends JFrame implements Observer {
    private SimulateurPotager simulateurPotager; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)

    private int sizeX; // taille de la grille affichée
    private int sizeY;

    private long tempsDebut = System.currentTimeMillis();
    private long tempsVar = System.currentTimeMillis();

    // icones affichées dans la grille
    private int co_x,co_y,co_w,co_h, de_x, de_y;
    private ImageIcon icoJeune;
    private ImageIcon icoPourris;
    private ImageIcon icoTerre;
    private ImageIcon icoVide;
    private ImageIcon icoMur;
    private Map<Ciel, ImageIcon> icoCiel = new HashMap<Ciel, ImageIcon>();
    private Map<Varietes, ImageIcon> icoMature = new HashMap<Varietes, ImageIcon>();
    private JPanel parametres = new JPanel();
    private JPanel details = new JPanel();
    private JLabel meteo = new JLabel();

    private Object[] infoVar;
    private Object[] paraVar;


    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associée à une icône, suivant ce qui est présent dans le modèle)


    public VueControleurPotager(SimulateurPotager _simulateurPotager) {
        sizeX = simulateurPotager.SIZE_X;
        sizeY = _simulateurPotager.SIZE_Y;
        simulateurPotager = _simulateurPotager;

        chargerLesIcones();
        placerLesComposantsGraphiques();
        //ajouterEcouteurClavier(); // si besoin
    }
/*
    private void ajouterEcouteurClavier() {
        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()) {  // on regarde quelle touche a été pressée
                    case KeyEvent.VK_LEFT : Controle4Directions.getInstance().setDirectionCourante(Direction.gauche); break;
                    case KeyEvent.VK_RIGHT : Controle4Directions.getInstance().setDirectionCourante(Direction.droite); break;
                    case KeyEvent.VK_DOWN : Controle4Directions.getInstance().setDirectionCourante(Direction.bas); break;
                    case KeyEvent.VK_UP : Controle4Directions.getInstance().setDirectionCourante(Direction.haut); break;
                }
            }
        });
    }
*/

    private void chargerLesIcones() {
    	// image libre de droits utilisée pour les légumes : https://www.vecteezy.com/vector-art/2559196-bundle-of-fruits-and-vegetables-icons


        //icoSalade = chargerIcone("Images/data.png", 0,0,120,120);//chargerIcone("Images/Pacman.png");
        //icoCarotte = chargerIcone("Images/data.png", 390, 390, 150, 150);
        icoJeune = chargerIcone("Images/croissance.png",13, 140, 150, 150);
        icoPourris = chargerIcone("Images/Fantome.png");
        icoVide = chargerIcone("Images/Vide.png");
        icoMur = chargerIcone("Images/Mur.png");
        icoTerre = chargerIcone("Images/Terre.png");
        Ciel[] ciels = Ciel.values();
        for(Ciel c: ciels){
            icoCiel.put(c,chargerIcone("Images/weather-forecast.png",c.getX(), c.getY(), c.getW(), c.getH()));
        }
        Varietes[] varietes = Varietes.values();
        for(Varietes v: varietes) {
            icoMature.put(v,chargerIcone("Images/data.png",v.getX(), v.getY(), v.getW(), v.getH()));
        }


    }

    private void placerLesComposantsGraphiques() {
        setTitle("A vegetable garden");
        setSize(640, 250);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permet de terminer l'application à la fermeture de la fenêtre

        /* JTextField jtf = new JTextField("infos diverses"); // TODO inclure dans mettreAJourAffichage ...
        jtf.setEditable(false);
        infos.add(jtf); */

        /* AJOUT D'ELEMENTS A METTRE SUR LA DROITE */

        BoxLayout boxLayout = new BoxLayout(details, BoxLayout.Y_AXIS);
        details.setLayout(boxLayout);
        add(details, BorderLayout.EAST);
        infoVar = new Object[6];
        infoVar[0] = new JLabel("                               ");
        infoVar[1] = new JLabel("");
        infoVar[2] = new JLabel("");
        infoVar[3] = new JLabel("");
        infoVar[4] = new JButton("Sauver");
        infoVar[5] = new JLabel("");

        details.add((JLabel) infoVar[0]);
        details.add((JLabel) infoVar[1]);
        details.add((JLabel) infoVar[2]);
        details.add((JLabel) infoVar[3]);
        details.add((JButton) infoVar[4]);
        details.add((JLabel) infoVar[5]);

        /* AJOUT D'ELEMENTS A METTRE EN DESSOUS */
        add(parametres, BorderLayout.SOUTH);

        Varietes[] varietes = Varietes.values();
        String[] var = new String[varietes.length];
        for(int i = 0; i < varietes.length; i++) {
            var[i] = varietes[i].name();
        }

        String[] modes = {"Edition", "Analyse"};

        /* Creation d'elements */
        paraVar = new Object[7];
        paraVar[0] = new JLabel(String.format("%.2f",simulateurPotager.simEco.getThune())+"$");
        paraVar[1] = new JButton("Edition");
        paraVar[2] = new JComboBox(var);
        paraVar[3] = new JButton("-");
        paraVar[4] = new JButton("+");
        paraVar[5] = new JLabel("vitesse");
        paraVar[6] = new JTextField("1", 6);
        ((JTextField) paraVar[6]).setEditable(false);
        /* JLabel lVitesse = new JLabel("vitesse");
        JTextField vitesse = new JTextField("1", 6);
        JLabel thune = new JLabel(String.valueOf(simulateurPotager.simEco.getThune())+"$");
        vitesse.setEditable(false);
        JComboBox mode = new JComboBox(modes);
        JComboBox varBox = new JComboBox(var); */
        /* JTextField bSalade = new JTextField("salade",6); */
        /* JButton moinsRapide = new JButton("-");
        JButton plusRapide = new JButton("+"); */
        /* System.out.println(Varietes.values()); */
        /* Ajout sur l'interface des elements créés */
        /* infos.add(bSalade); */
        parametres.add((JLabel) paraVar[0]);
        parametres.add((JButton) paraVar[1]);
        parametres.add((JComboBox) paraVar[2]);
        parametres.add((JButton) paraVar[3]);
        parametres.add((JButton) paraVar[4]);
        parametres.add((JLabel) paraVar[5]);
        parametres.add((JTextField) paraVar[6]);
        meteo.setIcon(icoCiel.get(simulateurPotager.simMet.getCiel()));
        parametres.add(meteo);



        ((JButton) paraVar[1]).addMouseListener( new MouseAdapter() {
            public void mouseClicked( MouseEvent m) {
                if(((JButton) paraVar[1]).getText() == "Edition")
                    ((JButton) paraVar[1]).setText("Analyse");
                else
                    ((JButton) paraVar[1]).setText("Edition");
            }
        });

        /* gestion des evenements liées aux boutons plus et moins pour gerer a vitesse de la simulation */
        ((JButton) paraVar[3]).addMouseListener( new MouseAdapter() {
            public void mouseClicked( MouseEvent m) {
                if(Ordonnanceur.getOrdonnanceur().getVitesse() > 0.25) {
                    Ordonnanceur.getOrdonnanceur().changeVitesse(0);
                    ((JTextField) paraVar[6]).setText(Float.toString(Float.parseFloat(((JTextField) paraVar[6]).getText())/2));
                }
            }
        });

        ((JButton) paraVar[4]).addMouseListener( new MouseAdapter() {
            public void mouseClicked( MouseEvent m) {
                if(Ordonnanceur.getOrdonnanceur().getVitesse() < 8) {
                    Ordonnanceur.getOrdonnanceur().changeVitesse(1);
                    ((JTextField) paraVar[6]).setText(Float.toString(Float.parseFloat(((JTextField) paraVar[6]).getText())*2));
                }
            }
        });

        ((JButton) infoVar[4]).addMouseListener( new MouseAdapter() {
            public void mouseClicked( MouseEvent m) {
                simulateurPotager.save.sauvegarder("save/s0", simulateurPotager);
                ((JLabel) infoVar[5]).setText("sauvé");
                tempsVar = System.currentTimeMillis();
            }
        });


        JComponent grilleJLabels = new JPanel(new GridLayout(sizeY, sizeX)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille

        tabJLabel = new JLabel[sizeX][sizeY];

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                JLabel jlab = new JLabel();

                tabJLabel[x][y] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);

            }
        }
        add(grilleJLabels, BorderLayout.CENTER);

        // écouter les évènements

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                final int xx = x; // constantes utiles au fonctionnement de la classe anonyme
                final int yy = y;
                tabJLabel[x][y].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        /* infoVar.setText("              ");
                        infoEtat.setText("              "); */
                        Legume legume = ((CaseCultivable) simulateurPotager.getPlateau()[xx][yy]).getLegume();
                        if(((JButton) paraVar[1]).getText() == "Edition") {
                            if(legume == null)
                                simulateurPotager.simEco.acheterGraine(Varietes.valueOf((String) ((JComboBox) paraVar[2]).getSelectedItem()));
                            else if(legume.getEtat() >= legume.getVariete().getCueillable() && legume.getEtat() < legume.getVariete().getPourris())
                                simulateurPotager.simEco.vendreLegume(Varietes.valueOf((String) ((JComboBox) paraVar[2]).getSelectedItem()));
                            ((JLabel) paraVar[0]).setText(String.format("%.2f",simulateurPotager.simEco.getThune())+"$");
                            simulateurPotager.actionUtilisateur(xx, yy, (String) ((JComboBox) paraVar[2]).getSelectedItem());
                        }
                        else {
                            de_x = xx;
                            de_y = yy;
                        }
                    }
                });
            }
        }
    }



    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     */
    private void mettreAJourAffichage() {
        Image image;
        /* if(mode.getSelectedItem() == "Edition") {

        } */
        meteo.setIcon(icoCiel.get(simulateurPotager.simMet.getCiel()));
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                if (simulateurPotager.getPlateau()[x][y] instanceof CaseCultivable) { // si la grille du modèle contient un Pacman, on associe l'icône Pacman du côté de la vue

                    Legume legume = ((CaseCultivable) simulateurPotager.getPlateau()[x][y]).getLegume();
                    /* Si aucun legume planté a l'emplacement */
                    if (legume != null) {
                        co_x=legume.getVariete().getX();
                        co_y=legume.getVariete().getY();
                        co_w=legume.getVariete().getW();
                        co_h=legume.getVariete().getH();
                        /* On verifie si les coordonnées de la case et les coordonnées de la case cliqué sont identiques */
                        if(de_x == x)
                            ((JLabel) infoVar[1]).setText("Variete : "+legume.getVariete()+"   ");
                        /* Si le legume et dans l'etat pourris */
                        if(legume.getEtat() >= legume.getVariete().getPourris()) {
                            tabJLabel[x][y].setIcon(icoPourris);
                            if(de_x == x)
                                ((JLabel) infoVar[2]).setText("etat : pourris");
                        }
                        /* Si le legume et dans l'etat mature */
                        else if(legume.getEtat() >= legume.getVariete().getCueillable()) {
                            tabJLabel[x][y].setIcon(icoMature.get(legume.getVariete()));
                            if(de_x == x) {
                                ((JLabel) infoVar[2]).setText("etat : mure");
                                ((JLabel) infoVar[3]).setText("temps avant pourris : "+(int) (legume.getVariete().getPourris()-legume.getEtat()));
                            }
                        }
                        /* Si le legume et une jeune pousse (donc pas encore mature) */
                        else{
                            tabJLabel[x][y].setIcon(icoJeune);
                            if(de_x == x) {
                                ((JLabel) infoVar[2]).setText("etat : jeune pousse");
                                ((JLabel) infoVar[3]).setText("temps avant pousse : "+(int) (legume.getVariete().getCueillable()-legume.getEtat()));
                            }
                        }
                    }
                    /* Si il y a dejà un legume à cet emplacement */
                    else {
                        tabJLabel[x][y].setIcon(icoTerre);
                    }

                    // si transparence : images avec canal alpha + dessins manuels (voir ci-dessous + créer composant qui redéfinie paint(Graphics g)), se documenter
                    //BufferedImage bi = getImage("Images/smick.png", 0, 0, 20, 20);
                    //tabJLabel[x][y].getGraphics().drawImage(bi, 0, 0, null);
                } else if (simulateurPotager.getPlateau()[x][y] instanceof CaseNonCultivable) {
                    tabJLabel[x][y].setIcon(icoMur);
                } else {

                    tabJLabel[x][y].setIcon(icoVide);
                }
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        mettreAJourAffichage();
        float vitesse = Ordonnanceur.getOrdonnanceur().getVitesse();
        //System.out.println(tempsDebut - System.currentTimeMillis());
        if(System.currentTimeMillis() - tempsDebut  > (1/vitesse)*5000){
            simulateurPotager.simMet.MiseAJour();
            meteo.setIcon(icoCiel.get(simulateurPotager.simMet.getCiel()));

            tempsDebut = System.currentTimeMillis(); // Mesurer le temps de début
        }
        if(((JLabel) infoVar[5]).getText() == "sauvé" && tempsVar+2000 < System.currentTimeMillis()) {
            ((JLabel) infoVar[5]).setText("");
        }
        /*
        SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        mettreAJourAffichage();
                    }
                });
        */

    }


    // chargement de l'image entière comme icone
    private ImageIcon chargerIcone(String urlIcone) {
        BufferedImage image = null;

        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleurPotager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }


        return new ImageIcon(image);
    }

    // chargement d'une sous partie de l'image
    private ImageIcon chargerIcone(String urlIcone, int x, int y, int w, int h) {
        // charger une sous partie de l'image à partir de ses coordonnées dans urlIcone
        BufferedImage bi = getSubImage(urlIcone, x, y, w, h);
        // adapter la taille de l'image a la taille du composant (ici : 20x20)
        return new ImageIcon(bi.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH));
    }

    private BufferedImage getSubImage(String urlIcone, int x, int y, int w, int h) {
        BufferedImage image = null;

        try {
            File f = new File(urlIcone);
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleurPotager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        BufferedImage bi = image.getSubimage(x, y, w, h);
        return bi;
    }

}
