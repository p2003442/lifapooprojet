
import modele.environnement.*;
import VueControleur.VueControleurPotager;
import modele.Ordonnanceur;
import modele.SimulateurPotager;
import modele.Sauvegarde;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class Main {
    public static void main(String[] args) {
        Sauvegarde s = new Sauvegarde();
        SimulateurPotager sim = s.charger("save/s0");
        SimulateurPotager simulateurPotager = new SimulateurPotager("save/s0");
        VueControleurPotager vc = new VueControleurPotager(simulateurPotager);
        vc.setVisible(true);
        Ordonnanceur.getOrdonnanceur().addObserver(vc);
        Ordonnanceur.getOrdonnanceur().start(100);
    }
}
