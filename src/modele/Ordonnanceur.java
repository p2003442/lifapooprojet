package modele;

import java.util.Observable;
import java.util.Vector;

import static java.lang.Thread.*;

public class Ordonnanceur extends Observable implements Runnable {

    private static Ordonnanceur ordonnanceur;

    // design pattern singleton
    public static Ordonnanceur getOrdonnanceur() {
        if (ordonnanceur == null) {
            ordonnanceur = new Ordonnanceur();
        }
        return ordonnanceur;
    }
    private long pause;
    private float vitesse;
    private Vector<Runnable> lst = new Vector<Runnable>(); // liste synchronisée

    /* Recuperer le temps de pause */
    public Long getPause() {
        System.out.println(pause);
        return pause;
    }

    public float getVitesse() {
        return vitesse;
    }

    public void changePause(int plusOuMoins) {
        if(plusOuMoins > 0)
            pause *=2;
        else if(pause > 100)
            pause /=2;

    }

    public void changeVitesse(int plusOuMoins) {
        if(plusOuMoins > 0)
            vitesse *=2;
        else if(vitesse > 0.25)
            vitesse /=2;
    }

    public void start(long _pause) {
        pause = _pause;
        vitesse = 1;
        new Thread(this).start();
    }

    public void add(Runnable r) {lst.add(r);}

    @Override
    public void run() {
        boolean update = true;

        while(true) {

            for (Runnable r : lst) {
                r.run();
            }

            if (update) {
                setChanged();
                notifyObservers();
                //update = false;
            }

            //update = true; // TODO : variable à déporter pour découpler le raffraichissement de la simulation
            try {
                sleep(pause);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
