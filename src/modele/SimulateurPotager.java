/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;


import modele.Meteo.Ciel;
import modele.Meteo.SimulateurMeteo;
import modele.environnement.Case;
import modele.environnement.CaseCultivable;
import modele.environnement.CaseNonCultivable;
import modele.environnement.Economie.SimulateurEconomique;

import java.awt.Point;
import java.io.Serializable;
import java.util.Random;


public class SimulateurPotager implements Serializable {

    public static final int SIZE_X = 20;
    public static final int SIZE_Y = 10;

    public SimulateurMeteo simMet;

    public Sauvegarde save;
    public SimulateurEconomique simEco;

    // private HashMap<Case, Point> map = new  HashMap<Case, Point>(); // permet de récupérer la position d'une entité à partir de sa référence
    private Case[][] grilleCases = new Case[SIZE_X][SIZE_Y]; // permet de récupérer une entité à partir de ses coordonnées
    public SimulateurPotager() {
        initialisationDesEntites();
        simMet = new SimulateurMeteo(this, Ciel.Soleil);
        simEco = new SimulateurEconomique(this);
        save = new Sauvegarde();
    }

    public SimulateurPotager(String charger) {
        initialisationDesEntites();
        save = new Sauvegarde();
        SimulateurPotager simpotTmp = new SimulateurPotager();
        simpotTmp = save.charger(charger);
        simMet = new SimulateurMeteo(simpotTmp, simpotTmp.simMet.getCiel());
        simEco = new SimulateurEconomique(simpotTmp,simpotTmp.simEco.getThune());
        /* grilleCases = simpotTmp.getPlateau(); */
        for (int x = 4; x <= 18; x++) {
            for (int y = 1; y <= 8; y++) {
                ((CaseCultivable) grilleCases[x][y]).setLegume(((CaseCultivable) simpotTmp.getPlateau()[x][y]).getLegume());

            }
        }

    }

    public Case[][] getPlateau() {
        return grilleCases;
    }

    private void initialisationDesEntites() {

        // murs extérieurs horizontaux
        for (int x = 0; x < 20; x++) {
            addEntite(new CaseNonCultivable(this), x, 0);
            addEntite(new CaseNonCultivable(this), x, 9);
        }

        // murs extérieurs verticaux
        for (int y = 1; y < 9; y++) {
            addEntite(new CaseNonCultivable(this), 0, y);
            addEntite(new CaseNonCultivable(this), 19, y);
        }

        addEntite(new CaseNonCultivable(this), 2, 6);
        addEntite(new CaseNonCultivable(this), 3, 6);

        for (int x = 4; x <= 18; x++) {
            for (int y = 1; y <= 8; y++) {
                CaseCultivable cc = new CaseCultivable(this);
                addEntite(cc , x, y);
                Ordonnanceur.getOrdonnanceur().add(cc);

            }
        }

    }

    public void actionUtilisateur(int x, int y, String aPlanter) {
        if (grilleCases[x][y] != null) {
            //System.out.print("ca plante ?");
            grilleCases[x][y].actionUtilisateur(aPlanter);
        }
    }

    private void addEntite(Case e, int x, int y) {
        grilleCases[x][y] = e;
        //map.put(e, new Point(x, y));
    }


    private Case objetALaPosition(Point p) {
        Case retour = null;
        return grilleCases[p.x][p.y];
    }

}
