package modele.Meteo;

public enum Ciel {
    Soleil(0, 0, 440, 440, 0.3, 1),
    eclaircies(490, 0, 450, 440, 0.3, 1),
    nuages(0, 500, 450, 320, 0.1, 0.5),
    pluie(490, 500, 450, 350, 0.1, 3),
    neige(0, 950, 420, 320, 0.05, 0.1),
    orage(490, 950, 400, 320, 0.05, 100);

    private final int x;
    private final int y;
    private final int w;
    private final int h;
    private final double p;

    private final double t;

    private Ciel(int x, int y, int w, int h, double p, double t) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.p = p;
        this.t = t;
    }

    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }
    public int getW() {
        return this.w;
    }
    public int getH() {
        return this.h;
    }
    public double getP(){
        return this.p;
    }
    public double getT(){
        return this.t;
    }

}
