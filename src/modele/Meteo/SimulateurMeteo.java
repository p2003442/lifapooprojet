package modele.Meteo;

import modele.Ordonnanceur;
import modele.SimulateurPotager;

import java.io.Serializable;
import java.util.Random;

public class SimulateurMeteo implements /* Runnable, */ Serializable {
    private SimulateurPotager simPot;
    long tempsDebut = System.currentTimeMillis(); // Mesurer le temps de début
    private Ciel nom = Ciel.Soleil;
    public SimulateurMeteo(SimulateurPotager _simPot, Ciel nom_) {
       // Ordonnanceur.getOrdonnanceur().add(this);
        simPot = _simPot;
        nom = nom_;

    }
    public Ciel getCiel() {
        return nom;
    }

    public void MiseAJour() {
        Ciel[] etats = Ciel.values();
        Random rand = new Random();
        double p = rand.nextDouble(); // génère un nombre aléatoire entre 0 et 1
        double cumul = 0.0;
        for (int i = 0; i < etats.length; i++) {
            cumul += etats[i].getP();
            if (p <= cumul) {
                nom = etats[i];
                System.out.println(nom.name());
                break;
            }
        }
        //System.out.println("update met");
    }
    /* @Override
    public void run() {
        float vitesse = Ordonnanceur.getOrdonnanceur().getVitesse();
        //System.out.println(tempsDebut - System.currentTimeMillis());
        if(System.currentTimeMillis() - tempsDebut  > (1/vitesse)*5000){
            simPot.simMet.MiseAJour();

            tempsDebut = System.currentTimeMillis(); // Mesurer le temps de début
        }
    } */
}


