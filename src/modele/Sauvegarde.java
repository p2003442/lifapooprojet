package modele;

import java.io.*;
import modele.SimulateurPotager;

public class Sauvegarde implements /* Runnable, */ Serializable {
    /* private long tempsDebut = System.currentTimeMillis(); */

    public Sauvegarde(){

    }
    public void sauvegarder(String nomFichier, SimulateurPotager simPot_) {
        try {
            FileOutputStream fichier = new FileOutputStream(nomFichier);
            ObjectOutputStream sortie = new ObjectOutputStream(fichier);
            sortie.writeObject(simPot_);
            sortie.close();/*
            fichier.close(); */
            //System.out.println("L'objet a été sauvegardé dans le fichier " + nomFichier);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public SimulateurPotager charger(String nomFichier) {
        SimulateurPotager potager = new SimulateurPotager();
        try {
            FileInputStream fichier = new FileInputStream(nomFichier);
            ObjectInputStream entree = new ObjectInputStream(fichier);
            potager = (SimulateurPotager) entree.readObject();
            entree.close();
            fichier.close();
            //System.out.println("L'objet a été chargé à partir du fichier " + nomFichier);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            sauvegarder(nomFichier, potager);
        }
        return potager;
    }
    /* @Override
    public void run() {
        float vitesse = Ordonnanceur.getOrdonnanceur().getVitesse();
        //System.out.println(tempsDebut - System.currentTimeMillis());
        if(System.currentTimeMillis() - tempsDebut  > (1/vitesse)*5000){
            simPot.save.sauvegarder("save/s0",simPot);

            tempsDebut = System.currentTimeMillis(); // Mesurer le temps de début
        }
    } */
}
