package modele.environnement;

import modele.SimulateurPotager;
//import modele.environnement.varietes.Carotte;
import modele.environnement.varietes.Legume;
//import modele.environnement.varietes.Salade;
import modele.environnement.varietes.Varietes;

import java.io.Serializable;
//import modele.environnement.varietes.unLegume;

public class CaseCultivable extends Case implements Serializable {

    private Legume legume;
    public CaseCultivable(SimulateurPotager _simulateurPotager) {
        super(_simulateurPotager);
    }

    @Override
    public void actionUtilisateur(String aPlanter) {
        if (legume == null) {
            /*switch (aPlanter){
                case "salade": legume = new Legume(Varietes.salade); break;
                case "carotte": legume = new Legume(Varietes.carotte); break;
                case "haricot": legume = new Legume(Varietes.haricot); break;
                case "poirreau": legume = new Legume(Varietes.poirreau);break;
            }*/

            //System.out.print("ca plante 2 ?");
            legume = new Legume(Varietes.valueOf(aPlanter));
            //legume = new Salade();

            //System.out.print("ca plante "+legume.getVariete());

        } else {
            legume = null;
        }
    }

    public Legume getLegume() {
        return legume;
    }

    public void setLegume(Legume legume_) {
        legume = legume_;
    }

    @Override
    public void run() {
        if (legume != null) {
            /* System.out.print("croissance"); */
            legume.croissance((float)simulateurPotager.simMet.getCiel().getT());
        }
    }
}
