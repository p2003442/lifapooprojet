package modele.environnement.Economie;
import modele.Ordonnanceur;
import modele.SimulateurPotager;
import modele.environnement.varietes.Varietes;

import java.io.Serializable;
import java.util.Random;
public class SimulateurEconomique implements Serializable {

    private SimulateurPotager simPot;
    private float thune;
    public SimulateurEconomique(SimulateurPotager _simPot){
        simPot = _simPot;
        thune=0;
    }
    public SimulateurEconomique(SimulateurPotager _simPot, float thune_){
        simPot = _simPot;
        thune=thune_;
    }
    public float getThune(){
        return thune;
    }
    public void acheterGraine(Varietes v){
        thune = thune - v.getPrix()/10;
    }

    public void vendreLegume(Varietes v){
        thune = thune + v.getPrix();
    }
}