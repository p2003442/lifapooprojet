package modele.environnement;

import modele.SimulateurPotager;

import java.io.Serializable;

public class CaseNonCultivable extends Case implements Serializable {
    public CaseNonCultivable(SimulateurPotager _simulateurPotager) {
        super(_simulateurPotager);
    }

    @Override
    public void actionUtilisateur(String aPlanter) {
        // TODO
    }

    @Override
    public void run() {
        // TODO
    }
}
