package modele.environnement.varietes;

import java.io.Serializable;

public enum Varietes implements Serializable {
    salade(0,0,120,120,16,64, 1),
    carotte(387,384,156,156,200,400, 1),
    poirreau(2697,0,160,160,16,64, 1),
    poivron(3120,0,160,160,16,64, 1),
    aubergine(768,372,160,160,16,64, 1),
    haricot(1176,400,150,150,16,64, 1),
    mais(1947,378,150,150,16,64, 1),
    cornichon(2334,380,150,150,16,64, 1),
    oignon(2718,375,150,150,16,64, 1),
    brocolie(3100,380,150,150,16,64, 1),
    ail(3500,380,150,150,16,64, 1),
    radis(770,770,150,150,16,64, 1),
    choux(1160,780,150,150,16,64, 1),
    mache(1550,1170,150,150,16,64, 1),
    tomates(3200,1170,150,150,16,64, 1),
    piment(1570,1550,150,150,16,64, 1);
    private final int x;
    private final int y;
    private final int w;
    private final int h;
    private final int cueillable;
    private final int pourris;
    private final float prix;
    private Varietes(int x, int y, int w, int h, int cueillable, int pourris, float prix) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.cueillable = cueillable;
        this.pourris = pourris;
        this.prix = prix;
    }
    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }
    public int getW() {
        return this.w;
    }
    public int getH() {
        return this.h;
    }
    public int getCueillable() {
        return this.cueillable;
    }
    public int getPourris() {
        return this.pourris;
    }
    public float getPrix() {
        return this.prix;
    }
}

