package modele.environnement.varietes;

import modele.Ordonnanceur;

import java.io.Serializable;

public class Legume implements Serializable {
    private Varietes nom;
    private double croissance=0;
    public Legume(Varietes nom_) {
        nom = nom_;
    }
    public Varietes getVariete() {
        return nom;
    }

    public double getEtat() {
        return croissance;
    }
    public void croissance(float t){
        float v = Ordonnanceur.getOrdonnanceur().getVitesse();
        if (croissance < nom.getPourris()) {
            croissance += 1*v*t;
        }
    }

}
